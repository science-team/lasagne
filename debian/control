Source: lasagne
Maintainer: Debian Science Maintainers <debian-science-maintainers@lists.alioth.debian.org>
Uploaders: Stephen Sinclair <radarsat1@gmail.com>
Section: science
Priority: optional
Rules-Requires-Root: no
Build-Depends: debhelper-compat (= 13),
               dh-python,
               python3-dev <!nocheck>,
               python3-setuptools,
               python3-theano,
               python3-numpy,
               links <!nodoc>,
               python3-pytest <!nocheck>,
               python3-mock <!nocheck>,
               python3-sphinx (>= 1.6.5~) <!nodoc>,
               python3-sphinx-rtd-theme <!nodoc>,
               python3-numpydoc <!nodoc>
Standards-Version: 4.5.0
Vcs-Browser: https://salsa.debian.org/science-team/lasagne
Vcs-Git: https://salsa.debian.org/science-team/lasagne.git
Homepage: https://lasagne.readthedocs.org/

Package: python3-lasagne
Architecture: all
Section: python
Depends: python3-theano,
         ${misc:Depends},
         ${python3:Depends}
Recommends: lasagne-doc
Description: deep learning library build on the top of Theano (Python3 modules)
 Lasagne is a Python library to build and train deep (multi-layered) artificial
 neural networks on the top of Theano (math expression compiler). In comparison
 to other abstraction layers for that like e.g. Keras, it abstracts Theano as
 little as possible.
 .
 Lasagne supports networks like Convolutional Neural Networks (CNN, mostly used
 for image recognition resp. classification) and the Long Short-Term Memory type
 (LSTM, a subtype of Recurrent Neural Networks, RNN).
 .
 This package contains the modules for Python 3.

Package: lasagne-doc
Architecture: all
Section: doc
Depends: ${misc:Depends},
         ${sphinxdoc:Depends},
         libjs-mathjax
Recommends: python3-lasagne
Description: deep learning Python library build on the top of Theano (docs)
 Lasagne is a Python library to build and train deep (multi-layered) artificial
 neural networks on the top of Theano (math expression compiler). In comparison
 to other abstraction layers for that like e.g. Keras, it abstracts Theano as
 little as possible.
 .
 Lasagne supports networks like Convolutional Neural Networks (CNN, mostly used
 for image recognition resp. classification) and the Long Short-Term Memory type
 (LSTM, a subtype of Recurrent Neural Networks, RNN).
 .
 This package contains the documentation.
Build-Profiles: <!nodoc>
