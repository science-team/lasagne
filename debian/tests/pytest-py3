#!/bin/sh

set -e

# Tests must use Theano backend with CPU device
export THEANO_FLAGS='device=cpu,cxx=,base_compiledir=/tmp'

# Go to tests directory
cd ${0%/*}/../../lasagne/tests
touch pytest.ini

# The objective here is not a complete integration test, but simply to
# verify that the package is working, thus we run a subset of unit
# tests that are particularly fast to run and should succeed if
# Lasagne is installed.
pytest-3 \
    test_init.py \
    test_nonlinearities.py \
    test_objectives.py \
    test_regularization.py \
    test_updates.py \
    test_utils.py \
    layers/test_base.py \
    layers/test_dense.py \
    layers/test_helper.py \
    layers/test_input.py \
    layers/test_shape.py \
    layers/test_special.py
